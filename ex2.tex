\documentclass[10pt,a4paper,usenames,dvipsnames,x10names]{article}
\usepackage{packs}

\author{
	Martino Barbieri (640522)\quad\texttt{m.barbieri20@studenti.unipi.it}
}
\title{Esercizio gruppo 2: l'equazione di Thomas-Fermi}
\date{Fisica 3: appello di giugno 2023}

\begin{document}
\maketitle
\thispagestyle{fancy}

\begin{abstract}
	\textbf{C.2.2--C.2.3} In questo esercizio ricaverò nelle ipotesi del modello
	di Thomas-Fermi, l'equazione che descrive la densità elettronica in funzione
	della distanza dal nucleo e calcolerò il raggio medio di un atomo nelle
	ipotesi del suddetto modello.
\end{abstract}

\section{Introduzione teorica}
Ai tempi di Fermi, risolvere numericamente l'eq.~di Schr\"{o}edinger per atomi
con molti elettroni era infattibile. Thomas e Fermi svilupparono un modello
approssimato semiclassico in grado di fornire una ragionevole approssimazione
della distribuzione degli elettroni attorno al nucleo.

\subsection{Ipotesi del modello}
Il loro modello prevedeva le seguenti ipotesi:
\begin{itemize}
	\item simmetria sferica;
	\item sistema stazionario;
	\item validità dello spazio delle fasi classico con misura
		\[
			\frac{\dd^3 \vb p\ \dd^3 \vb x}{h^3}\text{;}
		\]
	\item approssimazione non relativistica (si trascura l'effetto degli spin
		e l'energia cinetica degli elettroni viene calcolata con $\frac {p^2}{2m}$);
	\item campo elettromagnetico non quantizzato ma trattato in modo classico;
	\item temperatura prossima a zero, quindi usando il formalismo gran canonico
		\[
			\log Q = 2\int \frac{\dd^3 \vb p\ \dd^3 \vb x}{h^3}
			\log\left\{1+z \exp\left[-\beta\left(\frac{\vb p^2}{2m}-e\phi(\vb x)\right)\right]\right\}
			\text{,}
		\]
		\[
			n(\vb x) \simeq \frac{2}{h^3} \int \dd^3 \vb p\ %
				\theta\left(E\ped F-\frac{\vb p^2}{2m}+e\phi(\vb x)\right)
				\text{;}
		\]
		regolando opportunamente $E\ped F$ e le condizioni al bordo si può stimare
		pure il comportamento di atomi ionizzati.
	\item nel caso in esame, considero solo gli atomi neutri. Siccome $\phi\rightarrow 0$ rapidamente
		per $r\rightarrow+\infty$, ma la nube di elettroni non è completamente localizzata in una regione
		finita, impongo $E\ped F = 0$.
\end{itemize}
Queste ipotesi garantiscono un buon funzionamento quando il comportamento degli
elettroni può essere approssimato come una distribuzione continua di carica, ovvero quando $Z \gg 1$.

\subsection{I conti}
Introduciamo come equazione di autoconsistenza del modello l'equazione di Poisson per il potenziale
elettrico:
\[
	\laplacian\phi(\vb x) = -4\pi e(Z\delta(\vb x)-n(\vb x))\text{,}
\]
con le condizioni al contorno ($r=|\vb x|$)
\[\lim_{r\rightarrow 0} r\phi = Ze\quad\quad\lim_{r\rightarrow+\infty}r\phi=0\text{.}\]
Inoltre, dalle ipotesi sulla distribuzione degli elettroni, segue
\[
	n(\vb x) \simeq \frac{1}{3\pi^2}\left(\frac{2me\phi(\vb x)}{\hbar^2}\right)^{\frac 3 2}
	\text{.}
\]
Usando coordinate sferiche e unendo le precedenti equazioni si trova
\[
	\frac 1 r \pdv[2]{(r\phi)}{r}
	=\frac{4e}{3\pi}\left(\frac{2me}{\hbar^2}\right)^{\frac 32}\phi^{\frac32}\text{,}
\]
e introducendo le sostituzioni\footnote{$a_0=\hbar^2/me^2$ è il raggio di Bohr.}
$\phi=Ze\frac \chi r$, $\rho=\frac{r}{a_0}\left(\frac{8\sqrt{2}}{3\pi}\right)^{2/3}Z^{1/3}$,
	si trovano l'\textbf{equazione di Thomas-Fermi}, con le relative condizioni al bordo:
\[
	\dv[2]{\chi}{\rho}=\frac{\chi^{\frac 32}}{\sqrt{\rho}}\text{,}\quad\quad
	\chi(0)=1\text{,} \quad\quad \chi(+\infty)=0^+
	\text{.}
\]

\section{L'integrazione numerica}
Procediamo a integrare numericamente l'equazione differenziale. Userò uno script scritto in Python
e i moduli di \texttt{scipy} e \texttt{numpy}.

Lo script utilizzato è disponibile \href{https://gitlab.com/martino.barbieri/esercizi-fisica-3}{qui}.

\subsection{Accortezze utilizzate}
La soluzione può diventare negativa per alcuni parametri iniziali. Questo esce dal dominio (reale) di
$\chi''$. Per far andare a buon fine l'algoritmo, modifico l'eq.~differenziale e pongo $\chi''=0$ se
$\chi^{3/2}/\sqrt\rho$ restituisce valori singolari.

Di questa equazione, non abbiamo le condizioni iniziali, ma abbiamo delle boundary conditions. Per di
più, l'equazione è singolare in zero, dove abbiamo una delle condizioni; l'altra è all'infinito, quindi
non in un numero reale finito.

Per ovviare a questo problema, scelgo un punto iniziale intermedio (nello script ho usato 3), integro
numericamente la soluzione “in avanti” e “indietro” e cerco i parametri iniziali che danno il corretto
andamento, ovvero:
\begin{itemize}
	\item $\chi(0)=1$;
	\item $\chi$ strettamente decrescente (derivata negativa);
	\item $\chi$ strettamente positiva (per restare nel dominio dell'ODE).
\end{itemize}
Questa scelta è stata implementata usando \texttt{scipy.optimize.minimize} e usando come funzione di errore
la somma tra:
\begin{itemize}
	\item il termine $(\chi(0)-1)^2$;
	\item la somma delle derivate positive di $\chi$ divisa per il numero di punti di campionamento;
	\item l'opposto della somma dei valori negative di $\chi$ diviso per il numero di punti di campionamento.
\end{itemize}

\subsection{Comportamento di $\chi$ in funzione di $\chi'(0)$}
Riporto alcuni grafici ottenuti in fig.~\ref{fig:solutions}.
\begin{figure}[h]
	\centering
	\includesvg[width=0.8\textwidth]{thomas-fermi-solutions}
	\caption{Alcune soluzioni dell'equazione di Thomas-Fermi. Variando di
	poco il valore di $\chi'(0)$, il comportamento all'infinito di $\chi$
	cambia parecchio.}
	\label{fig:solutions}
\end{figure}
Si nota che il corretto comportamento di $\chi$, si ha per
$\chi'(0)\simeq-1.59$. Per valori maggiori, la soluzione cresce all'infinito;
per valori di poco minori invece la funzione raggiunge il valore 0, quindi
sarebbe da raccordare con la funzione costante 0. In quest'ultimo caso si può
interpretare quel raggio come il raggio dell'atomo ionizzato negativamente.

\section{Risultati}
Riporto in fig.~\ref{fig:results} il grafico di $4\pi r^2 n(r)$, in unità di
misura naturali.
\begin{figure}[h]
	\centering
	\includesvg[width=0.8\textwidth]{thomas-fermi-distro}
	\caption{Distribuzione di carica ricavata dal modello di Thomas-Fermi.
	È indicata in azzurro un'area pari alla metà dell'area totale sotto
	la curva, quindi l'ascissa del bordo destro di tale area rappresenta
	la stima del raggio atomico medio.}
	\label{fig:results}
\end{figure}
Il raggio medio $\langle r \rangle$ è quello la cui sfera corrispondente
racchiude metà della nube elettronica atomica. Questo si stima imponendo
\[
	\int_0^{\langle r \rangle} 4\pi n(r) r^2 \dd r = \frac{Z}{2}
\]
e numericamente, lo script restituisce
\[
	\langle r \rangle = 1.67\ a_0 Z^{-\frac 1 3}\text{.}
\]
Nel caso $Z=12$ del magnesio, questo corrisponde a una stima di
$\SI{0.386}{\angstrom}$, che corrisponde circa al doppio dei valori tabulati,
che stanno tra i 100 e i $\SI{200}{pm}$.

\end{document}
