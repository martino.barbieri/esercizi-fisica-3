import numpy as np
from scipy.integrate import odeint
from scipy.optimize import minimize
import matplotlib.pyplot as plt

# PARTE PRIMA: Integrare l'equazione differenziale

# modello di Thomas-Fermi
def model(phi,rho):
    return (phi[1],np.nan_to_num(np.sqrt(phi[0]**3/rho), nan=0, posinf=10, neginf=-10))
    #return (phi[1],-phi[0])

def solve(p0,p0d,max_t=10,min_t=0,density=100):
    # initial condition
    y0 = (p0,p0d)

    # time points
    tp = np.linspace(3,max_t,density*(max_t-3))
    tn = np.linspace(3,min_t,density*3)

    # solve ODE
    yp = odeint(model,y0,tp)
    yn = odeint(model,y0,tn)

    yyp, yypd = np.transpose(yp)
    yyn, yynd = np.transpose(yn)
    yyn  = np.flip(yyn)
    yynd = np.flip(yynd)
    tn = np.flip(tn)

    y  = np.concatenate((yyn,yyp))
    yd = np.concatenate((yynd,yypd))
    t  = np.concatenate((tn,tp))

    return (t,y,yd)

def error(P0):
    p0,p0d = P0
    s = solve(p0,p0d,max_t=100,min_t=0,density=100000)
    zeri = np.zeros_like(s[0])
    ret = (1-s[1][0])**2 + np.sum( np.maximum(zeri,-s[1]) + np.maximum(zeri,s[2]) ) / len(s[0])
    print(ret)
    return ret

popt = minimize(error, [0.1555,-0.0632], bounds=[(0.13,0.17),(-0.08,-0.04)])

print(popt.success)
print(popt.message)
print(popt.x)

t,y,yd = solve(*(popt.x),max_t=10,density=10000)

print(y[0],yd[0])

ym = np.transpose(odeint(model,(1,yd[0]+0.01),t))[0]
yl = np.transpose(odeint(model,(1,yd[0]-0.01),t))[0]

# bellurie grafiche
plt.rcParams["ps.usedistiller"] = 'xpdf'
plt.rcParams["text.usetex"] = True

fig = plt.figure("Soluzioni dell'equazione di Thomas-Fermi")
plt.xlabel(r'$\rho$')
plt.ylabel(r'$\chi$')
plt.grid(which='both', ls='dashed', color='gray')

# plot results
plt.plot(t,y,label=r"$\chi'(0)$ corretto")
plt.plot(t,ym,label=r"$\chi'(0)$ aumentato di 0.01")
plt.plot(t,yl,label=r"$\chi'(0)$ diminuito di 0.01")
plt.ylim([-0.2,1.2])
plt.legend()
plt.show()

# PARTE SECONDA: Trovare il raggio medio
fig = plt.figure("Distribuzione di Thomas-Fermi")
plt.xlabel(r'$r \left[a_0 Z^{-1/3}\right]$')
plt.ylabel(r'$4\pi r^2 n \left[Z^{4/3} a_0^{-1}\right]$')
plt.grid(which='both', ls='dashed', color='gray')

r = t * (3*np.pi/(128**0.5))**(2/3)
nr24pi = 2**1.5/(3*np.pi**2) * y**1.5 * r**0.5 * 4*np.pi

# integrale con trapezi
prob = np.zeros_like(r)
prob[0] = 0.5*nr24pi[0]*(r[1]-r[0])

for i in range(1,len(r)):
    prob[i] = prob[i-1] + (r[i]-r[i-1]) * (nr24pi[i-1]+nr24pi[i])/2
    if prob[i] > 0.5 and prob[i-1] <= 0.5:
        # si potrebbe fare una media ponderata, ma per una stima
        #  non è necessario
        print("raggio medio: ",(r[i]+r[i-2])/2)

# plot results
plt.plot(r,nr24pi)
plt.fill_between(r,nr24pi,0,where=(prob < 0.5),alpha=0.5 )
plt.show()
