import numpy as np
from scipy.integrate import quad as integra
import matplotlib.pyplot as plt

# bellurie grafiche
plt.rcParams["ps.usedistiller"] = 'xpdf'
plt.rcParams["text.usetex"] = True

# PARTE PRIMA sez urt diff in funzione dell'energia

def omega1(omega,theta):
    return omega / (1+omega*(1-np.cos(theta)))

def sigmaintheta(omega,theta):
    o1 = omega1(omega,theta)
    return np.pi*(o1/omega)**2*(o1/omega+omega/o1-np.sin(theta)**2)*np.sin(theta)

def sigmainomega(omega,theta):
    o1 = omega1(omega,theta)
    return 0.5*(o1/omega)**2*(o1/omega+omega/o1-np.sin(theta)**2)

def sigmatot(omega):
    ret = [integra(lambda x: sigmaintheta(w,x),0,np.pi)[0] for w in omega]
    return ret

def sigmatotanal(omega):
    return np.pi/omega**3*((omega**2-2*omega-2)*np.log(1+2*omega)+2*omega/(1+2*omega)**2*(omega**3+9*omega**2+8*omega+2))

# in dOmega

fig = plt.figure("Sezioni d'urto differenziali dello scattering Compton")

plt.xlabel(r'$\theta\ [\pi]$')
plt.ylabel(r'd$\sigma$/d$\Omega\ [r_e^2]$')
plt.grid(which='both', ls='dashed', color='gray')

theta=np.linspace(0,np.pi,1000)

plt.plot(theta/np.pi,(1+np.cos(theta)**2)/2,label=r"$\omega \left[m\right]$ = 0")
# plot results
for w in np.logspace(-2,+3,7):
    plt.plot(theta/np.pi,sigmainomega(w,theta),label=r"$\omega \left[m\right]$ = "+f'{w:.3f}')
plt.legend()
plt.yscale("log")
plt.ylim([1e-4,10])
plt.show()

# in dTheta

fig = plt.figure("Sezioni d'urto differenziali dello scattering Compton")
plt.xlabel(r'$\theta\ [\pi]$')
plt.ylabel(r'd$\sigma$/d$\theta\ [r_e^2]$')
plt.grid(which='both', ls='dashed', color='gray')

theta=np.linspace(0,np.pi,1000)

plt.plot(theta/np.pi,np.pi*(1+np.cos(theta)**2)*np.sin(theta),label=r"$\omega \left[m\right]$ = 0")
# plot results
for w in np.logspace(-2,+3,7):
    plt.plot(theta/np.pi,sigmaintheta(w,theta),label=r"$\omega \left[m\right]$ = "+f'{w:.3f}')
plt.legend()
plt.yscale("log")
plt.ylim([1e-4,10])
plt.show()

# PARTE SECONDA sez urt tot in funzione dell'energia

fig = plt.figure("Sezioni d'urto totali dello scattering Compton")
gs = fig.add_gridspec(2,1, hspace=0, height_ratios=[0.8,0.2])
axs = gs.subplots(sharex=True)
plt.xlabel(r'$\omega\ [m]$')
axs[0].set(ylabel=r'$\sigma\ [r_e^2]$',yscale="log")
axs[1].set(ylabel=r'residui [$10^{-8}$]')
axs[1].ticklabel_format(style='plain',axis='y')
axs[0].grid(which='both', ls='dashed', color='gray')
axs[1].grid(which='both', ls='dashed', color='gray')

w=np.logspace(-3,3,1000)
s=sigmatot(w)

# plot results
axs[0].plot(w,sigmatot(w),label="Valutato numericamente",ls=(0,(10,10)))
axs[0].plot(w,sigmatotanal(w),label="Espressione analitica",ls=(10,(10,10)))
axs[0].legend()
axs[1].plot(w,1e8*(sigmatot(w)-sigmatotanal(w))/sigmatotanal(w))
plt.xscale("log")
plt.show()
