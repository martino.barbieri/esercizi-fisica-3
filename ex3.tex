\documentclass[10pt,a4paper,usenames,dvipsnames,x10names]{article}
\usepackage{packs}

\author{
	Martino Barbieri (640522)\quad\texttt{m.barbieri20@studenti.unipi.it}
}
\title{Esercizio gruppo 3: scattering Compton ad alte energie}
\date{Fisica 3: appello di giugno 2023}

\begin{document}
\maketitle
\thispagestyle{fancy}

\begin{abstract}
	\textbf{C.3.16--dispense pag.~89} Utilizzando la formula della sezione d'urto differenziale per
	l'effetto Compton ricavata dalla QED, calcoliamo la sezione d'urto totale.
\end{abstract}

\section{Introduzione teorica}
La sezione d'urto differenziale per l'effetto Compton è
\[
	\dv{\sigma}{\Omega} = \frac{r_e^2}{2}\left(\frac{\omega'}{\omega}\right)^2
	\left(\frac{\omega'}{\omega}+\frac{\omega}{\omega'}-\sin^2\theta\right)\text{,}
\]
dove $\theta$ è l'angolo di scattering del fotone, $\omega,\ \omega'$ sono rispettivamente le frequenze
del fotone prima e dopo l'urto e $r_e=\frac{e^2}{mc^2}$ è il raggio classico dell'elettrone.

\subsection{Energia del fotone scatterato}
Calcoliamo preliminarmente l'energia del fotone diffuso, nel caso in cui l'elettrone sia inizialmente fermo,
approssimazione valida per $\omega \gg \SI{10}{eV}$, ovvero per energie maggiori dell'energia di legame
degli elettroni. Per far ciò, scriviamo la conservazione del
quadrimpulso totale. Indico con le grandezze primate quelle successive all'urto, con pedice $\gamma$ le
grandezze relative al fotone e con $e$ quelle relative all'elettrone:
\[
	p_\gamma + p_e = p_\gamma' + p_e'\text{,}
\]
\[
	p_\gamma + p_e - p_\gamma' = p_e'\text{.}
\]
Quadrando membro a membro e semplificando i termini che si annullano, si ottiene
\[
	p_\gamma p_e = p_\gamma p_\gamma' + p_\gamma' p_e\text{,}
\]
da cui è immediato (usando unità naturali $c=\hbar=e=1$)
\[
	\omega' = \frac{\omega}{1+\frac{\omega}{m}(1-\cos\theta)}
	\text{.}
\]
Dalla conservazione dell'energia, ricaviamo l'energia cinetica trasferita all'elettrone:
\[
	T=\omega\frac{\frac{\omega}{m}(1-\cos\theta)}{1+\frac{\omega}{m}(1-\cos\theta)}\le
	\frac{2\omega^2}{m+2\omega}\text{.}
\]

\subsection{Basse energie}
A basse energie, ovvero per $\omega \ll m$, si ha $\omega' \simeq \omega$; nel primo ordine in $\omega/m$,
risulta
\[
	\dv{\sigma}{\Omega} \simeq {r_e^2}\left(1-2\frac\omega m (1-\cos\theta)\right)
	\left(1-\frac{\sin^2\theta}2\right)
	= {r_e^2}\left(1-2\frac\omega m (1-\cos\theta)\right)
	\left(\frac{1+\cos^2\theta}2\right)
	\text{,}
\]
che vediamo essere la sezione d'urto Thomson a meno di una correzione nell'ordine di $\omega/m$.

\section{Sezione d'urto angolare}
Usando la simmetria cilindrica attorno all'asse di incidenza del fascio fotonico,
\[
\dd \Omega = \sin\theta\dd\theta\dd\phi=2\pi\sin\theta\dd\theta\text{.}
\]
È quindi immediato ricavare
\[
	\dv{\sigma}{\theta} = r_e^2\pi\left(\frac{\omega'}{\omega}\right)^2
	\left(\frac{\omega'}{\omega}+\frac{\omega}{\omega'}-1+\cos^2\theta\right)\sin\theta\text{.}
\]

\subsection{Integrale analitico}
Questa espressione si riesce a integrare analiticamente: introducendo le sostituzioni
$\eta = \frac\omega m$,\\$\xi = 1+\eta(1-\cos\theta)$ si ottiene
\begin{align*}
	\sigma &= \int_{0}^{\pi} \dv{\sigma}{\theta} \dd \theta\\
		   &= \frac{r_e^2\pi}{\eta}
		   \int_{1}^{2\eta+1} \left[ \xi^{-3} +
			\left(\frac 2\eta+\frac 1{\eta^2}\right)\xi^{-2}+
			\left(1-\frac2\eta-\frac2{\eta^2}\right)\xi^{-1}+\frac1{\eta^2}
		   \right]\dd\xi\\
		   &=\frac{\pi r_e^2}{\eta^3}\left[
				(\eta^2-2\eta-2)\log(1+2\eta)+\frac{2\eta}{(1+2\eta)^2}(\eta^3+9\eta^2+8\eta+2)
			\right]\text{.}
\end{align*}
Identicamente, possiamo ricavare l'energia media del fotone diffuso:
\begin{align*}
	\frac{\sigma\langle\omega'\rangle}{\omega}
	&= \int_{0}^{\pi} \dv{\sigma}{\theta}\frac{\omega'}{\omega} \dd \theta\\
		   &= \frac{r_e^2\pi}{\eta}
		   \int_{1}^{2\eta+1} \left[ \xi^{-4} +
			\left(\frac 2\eta+\frac 1{\eta^2}\right)\xi^{-3}+
			\left(1-\frac2\eta-\frac2{\eta^2}\right)\xi^{-2}+\frac1{\eta^2}\xi^{-1}
		   \right]\dd\xi\text{,}
\end{align*}
da cui
\[
	\langle\omega'\rangle = \omega \frac{
		3(1+2\eta)^3\log(1+2\eta)+32\eta^5+12\eta^4-36\eta^3-30\eta^2-6\eta
	}{
		3(\eta^2-2\eta-2)(1+2\eta)^3\log(1+2\eta)+6\eta(1+2\eta)(\eta^3+9\eta^2+8\eta+2\eta)
	}
	\text{.}
\]

\section{Calcoli numerici e conclusioni}
Condivido tutti gli script utilizzati \href{https://gitlab.com/martino.barbieri/esercizi-fisica-3}{qui}.
Numericamente, otteniamo i grafici delle sezioni d'urto differenziali per angolo solido (fig.~\ref{fig:solid}) e
per angolo di scattering (fig.~\ref{fig:theta}) a diverse frequenze (misurate in unità di $m$); si nota che, come dimostrato
algebricamente, a basse energie, le sezioni d'urto differenziali tendono alle sezioni Thomson.
\begin{figure}[h]
	\centering
	\includesvg[width=0.8\textwidth]{compton-diff-omega}
	\caption{Sezione d'urto differenziale per unità di angolo solido a diverse energie.}
	\label{fig:solid}
\end{figure}
\begin{figure}[h]
	\centering
	\includesvg[width=0.8\textwidth]{compton-diff-theta}
	\caption{Sezione d'urto differenziale per unità di angolo di scattering a diverse energie.}
	\label{fig:theta}
\end{figure}
\begin{figure}[h]
	\centering
	\includesvg[width=0.8\textwidth]{compton-tot}
	\caption{Sezione d'urto totale in funzione dell'energia, calcolata integrando numericamente e dall'espressione analitica.}
	\label{fig:tot}
\end{figure}
Riporto anche in fig.~\ref{fig:tot} le sezioni d'urto totali in funzione dell'energia, calcolate tramite l'espressione ricavata precedentemente e integrando
numericamente. I residui normalizzati sono piccolissimi (stanno su $10^{-7}$ per basse energie e poi si abbassano di parecchio) e le curve sono praticamente
indistinguibili. A basse energie, la sezione d'urto tende a stabilizzarsi alla sezione d'urto totale di Thomson anche in questo caso.

\end{document}
