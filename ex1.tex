\documentclass[10pt,a4paper,usenames,dvipsnames,x10names]{article}
\usepackage{packs}

\author{
	Martino Barbieri (640522)\quad\texttt{m.barbieri20@studenti.unipi.it}
}
\title{Esercizio gruppo 1: resistenza di irraggiamento}
\date{Fisica 3: appello di giugno 2023}

\begin{document}
\maketitle
\thispagestyle{fancy}

\begin{abstract}
	\textbf{C.1.12} In questo esercizio ricaverò la resistenza di irraggiamento di un circuito $RC$.
\end{abstract}

Abbiamo un circuito quadrato di lato $L$, piccolo rispetto alla lunghezza d'onda $\lambda$ della
radiazione monocromatica incidente e propagante lungo $\hat z$.
Il circuito è $RC$ ed il condensatore è piano con area $A$ e distanza
$d$ tra le piastre. Assumiamo che la polarizzazione dell'onda stia nel piano del circuito.
Suppongo inoltre che il condensatore abbia faccie ortogonali al versore $\hat n$.

\section{Dipolo elettrico}
Nell'ipotesi $\lambda \gg L$, possiamo considerare uniforme la corrente nel circuito.
Non possiamo invece supporre che valga la legge delle maglie, perché, per la legge di Faraday-Neumann-Lenz,
vi è una f.e.m.~indotta nel circuito a causa della variazione di campo magnetico.
Diciamo che, per l'onda incidente
\[
	\vb B \simeq B_0 \cos\omega t \hat y = B_0 \Re e^{-i\omega t} \hat y\text{,}
\]
con $\omega=2\pi c/\lambda$ e, conseguentemente:
\[
	\mathcal E = L^2 B_0 \omega \sin \omega t = L^2 B_0 \omega \Re (-i e^{-i\omega t})\text{.}
\]
Possiamo supporre che la corrente, a regime, oscilli in modo monocromatico, con la stessa frequenza $\omega$
della radiazione incidente.
Conseguentemente, passando in notazione complessa (tutto evolve proporzionalmente a $e^{-i\omega t}$,
la carica sulle armature del condensatore è, per l'equazione di continuità\footnote{Prendendo la corrente
positiva in senso antiorario, $I=-\dv{Q}{t}$.}
\[
	Q=-\frac i{\omega} I\text{.}
\]
Il dipolo elettrico totale del sistema è quindi
\[
	\vb d = Qd\ \hat n=-\frac{i}{\omega}Id\ \hat n\text{.}
\]

\section{Dipolo magnetico}
Nel circuito scorre una corrente $I$, quindi il dipolo magnetico è
\[
	\boldsymbol{\mu} = L^2 I\ \hat y\text{.}
\]

\section{Quadrupolo elettrico}
Dobbiamo tenere in conto l'irraggiamento di quadrupolo. Tuttavia, per una trasformazione di inversione spaziale
$C+\vb r\rightarrow C-\vb r$, dove $C$ è il centro geometrico del condensatore,
$\rho(\vb r)\rightarrow-\rho(\vb r)$ e il momento di quadrupolo $Q_{ij}$ è lineare nella carica, quindi
$Q_{ij}\rightarrow-Q_{ij}$. Tuttavia, il momento di quadrupolo è pari, quindi non cambia segno sotto
inversione spaziale. Segue $Q_{ij}=-Q_{ij}$, ovvero il momento di quadrupolo è identicamente nullo.

Momenti multipolari di ordine superiore possono essere trascurati perché i loro effetti sono sopressi da molte
potenze di $c$.

\section{I campi}
I campi prodotti da un dipolo osservato lungo $\hat m$ in zona di far field sono:
\begin{itemize}
	\item per un dipolo elettrico $\vb p$
		\begin{align*}
			\vb H\ped e = &\frac{c k^2}{4\pi} (\hat m\wedge \vb p)\frac{e^{-i(\omega t-kr)}}r\text{,} \\
			\vb E\ped e = -Z_0 &\frac{c k^2}{4\pi} \hat m
			\wedge (\hat m\wedge \vb p)\frac{e^{-i(\omega t-kr)}}r\text{;}
		\end{align*}
	\item per un dipolo magnetico $\boldsymbol{\mu}$
		\begin{align*}
			\vb H\ped m = -&\frac{k^2}{4\pi} \hat m \wedge(\hat m\wedge\boldsymbol{\mu})\frac{e^{-i(\omega t-kr)}}r\text{,} \\
			\vb E\ped m = -Z_0 &\frac{k^2}{4\pi} (\hat m\wedge\boldsymbol{\mu})\frac{e^{-i(\omega t-kr)}}r\text{.}
		\end{align*}
\end{itemize}

I campi totali sono quindi
\begin{align*}
	\vb H\ped{tot} =     & \frac{k^2}{4\pi}\frac{e^{-i(\omega t-kr)}}r \hat m\wedge\left[c\vb p-\hat m\wedge\boldsymbol{\mu}\right]\text{,} \\
	\vb E\ped{tot} = Z_0 & \frac{k^2}{4\pi}\frac{e^{-i(\omega t-kr)}}r \hat m\wedge\left[-c\hat m\wedge\vb p-\boldsymbol{\mu}\right]\text{,}
\end{align*}
ovvero, in termini degli elementi circuitali,
\begin{align*}
	\vb H\ped{tot} =     & \frac{k^2}{4\pi}\frac{e^{-i(\omega t-kr)}}r \hat m\wedge\left[-\frac{i}{\omega}Idc\hat n-L^2 I\hat m\wedge\hat y\right]\text{,} \\
	\vb E\ped{tot} = Z_0 & \frac{k^2}{4\pi}\frac{e^{-i(\omega t-kr)}}r \hat m\wedge\left[\frac{i}{\omega}Idc\hat m\wedge\hat n-L^2 I\hat y\right]\text{.}
\end{align*}

\section{Il vettore di Poynting}
Per calcolare il vettore di Poynting medio, dovremmo fare la media temporale di $\vb S\ped{tot} = \vb E\ped{tot} \wedge \vb H\ped{tot}$.
Qui avviene una bellissima semplificazione:
\[
	\vb S\ped{tot} = \vb S\ped e + \vb S\ped m + \vb E\ped e\wedge \vb H\ped m + \vb E\ped m\wedge \vb H\ped e\text{.}
\]
Usando, per la media di grandezze oscillanti in modo complesso $a=a_0 e^{-i\omega t},\ b=b_0 e^{-i\omega t}$, la formula
\[
	\langle \Re a \Re b \rangle = \frac 1 2 \Re (a_0^*b_0)\text{,}
\]
si ottiene che i termini misti si cancellano, quindi in questo caso il vettore di Poynting è la somma dei vettori di Poynting.
Ragionamento analogo si potrebbe fare per un circuito $RL$, ma non per un $RLC$, in quanto induttore e capacitore sarebbero in fase e farebbero interferenza.

Dunque pure le potenze (e quindi le resistenze di irraggiamento e le sezioni d'urto) si sommano semplicemente.

La potenza irradiata è
\begin{align*}
	& P\ped e=\frac{c^2Z_0}{12\pi}\frac{k^4}{\omega^2} d^2 |I|^2=\frac{Z_0}{6\pi}k^2 d^2 I^2\text{,} \\
	& P\ped m=\frac{Z_0}{12\pi}k^4 L^4 |I|^2=\frac{Z_0}{6\pi}k^4 L^4 I^2\text{.}
\end{align*}

\section{Le resistenze d'irraggiamento}
L'energia media dissipata da un resistore è $RI^2$, quindi possiamo associare all'irraggiameno due resistenze $R\ped e,\ R\ped m$ che modellizzano la
reazione dinamica del campo elettromagnetico.
\[R\ped e = \frac{Z_0}{6\pi}k^2 d^2, \quad\quad R\ped m = \frac{Z_0}{6\pi}k^4 L^4\text{.}\]

\section{La corrente nel circuito RC}
Il capacitore ha capacità
\[
	C=\varepsilon_0 \frac{A}{d}\text{,}
\]
quindi usando il metodo delle impedenze complesse si può trovare la corrente che scorre nel circuito:
\[
	I=\frac{\mathcal{E}}{R+R\ped e+R\ped m-\frac{i}{\omega C}}\text{,}
\]
da cui, usando
\begin{align*}
	& I^2=\frac 1 2 \frac{B_0^2L^4\omega^2}{(R+R\ped e+R\ped m)^2+\frac{d^2}{\omega^2 \varepsilon_0^2 A^2}}\text{,}\\
	& \therefore I^2 =S \frac{\frac{\mu_0 L^4\omega^2}c}{\left(R+\frac{Z_0}{6\pi c^2}\omega^2 k^2 d^2 +\frac{Z_0}{6 \pi c^4} \omega^4 L^4\right)^2+\frac{d^2}{\omega^2 \varepsilon_0^2 A^2}}\text{.}\\
\end{align*}

\section{Sezioni d'urto}
Per la sezione d'urto totale, basta applicare la definizione:
\[
	\sigma\ped{abs} = \frac {RI^2}{S} = \frac{R\frac{\mu_0 L^4\omega^2}c}{\left(R+\frac{Z_0}{6\pi c^2}\omega^2 k^2 d^2 +\frac{Z_0}{6 \pi c^4} \omega^4 L^4\right)^2+\frac{d^2}{\omega^2 \varepsilon_0^2 A^2}}\text{,}\\
\]
\[
	\sigma\ped{e} = \frac {R\ped e I^2}{S} = \frac{\frac{Z_0}{6\pi c^2}\omega^2 k^2 d^2\frac{\mu_0 L^4\omega^2}c}{\left(R+\frac{Z_0}{6\pi c^2}\omega^2 k^2 d^2 +\frac{Z_0}{6 \pi c^4} \omega^4 L^4\right)^2+\frac{d^2}{\omega^2 \varepsilon_0^2 A^2}}\text{,}\\
\]
\[
	\sigma\ped{m} = \frac {R\ped m I^2}{S} = \frac{\frac{Z_0}{6\pi c^4}\omega^2 k^4 L^4\frac{\mu_0 L^4\omega^2}c}{\left(R+\frac{Z_0}{6\pi c^2}\omega^2 k^2 d^2 +\frac{Z_0}{6 \pi c^4} \omega^4 L^4\right)^2+\frac{d^2}{\omega^2 \varepsilon_0^2 A^2}}\text{,}\\
\]
mentre per le sezioni d'urto differenziali, uso $\sin^2\alpha=1-\cos^2\alpha$:
\[
	\dv{\sigma\ped e}{\Omega}=\frac{3}{8\pi}\sigma\ped e \sin^2\alpha=\frac{3}{8\pi}\sigma\ped e (1-(\hat m\cdot\hat n)^2)\text{,}
\]
\[
	\dv{\sigma\ped m}{\Omega}=\frac{3}{8\pi}\sigma\ped m \sin^2\alpha=\frac{3}{8\pi}\sigma\ped m (1-(\hat m\cdot\hat y)^2)\text{.}
\]

\end{document}
